<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
?>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=($this->uri->segment(1) == 'TugasAkhir')?'active':''?>"><a href="<?=site_url('Welcome/da
shboard')?>"><i class="fa fa-home"></i> <span>Dashboard</span> </a></li>

  <li class="treeview <?=($this->uri->segment(1) == 'Feedback'
    || $this->uri->segment(1) == 'Jenis'
    || $this->uri->segment(1) == 'Kategori'
    || $this->uri->segment(1) == 'User')?'active':''?>">
    <a href="#">
      <i class="fa fa-gear"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li class="<?=($this->uri->segment(1) == 'Kategori')?'active':''?>"><a href="<?=site_url('Kategori')?>"><span>Kategori</span></a></li>
      <li class="<?=($this->uri->segment(1) == 'User')?'active':''?>"><a href="<?=site_url('User')?>"><span>User</span></a></li>
    </ul>
  </li>
  <li class="<?=($this->uri->segment(1) == 'Forecast')?'active':''?>"><a href="<?=site_url('Forecast')?>"><i class="fa fa-magic"></i> <span>Forecast</span></a></li>
</ul>