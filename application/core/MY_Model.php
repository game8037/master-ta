<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	function __construct(){
		parent::__construct();
		if (!isset($this->pk)){
			$this->pk = $this->table_name."id";
		}
	}

    function getAll($order = 'DESC') {
        $this->db->order_by($this->pk, $order);
        $query = $this->db->get($this->table_name);
        return $query->result();
    }
    
    function getAllBy($kondisi, $order = 'DESC') {
        $this->db->order_by($this->pk, $order);
        $query = $this->db->get_where($this->table_name, $kondisi);
        return $query->result();
    }
    
    function getLast() {
        $this->db->order_by($this->pk, "DESC");
        $query = $this->db->limit(1)->get($this->table_name);
        return $query->row();
    }
    
    function getDetail($id) {
        $this->db->where($this->pk, $id);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function add($data) {
        $this->db->insert($this->table_name, $data);
    }
    
    function update($id, $data) {
        $this->db->where($this->pk, $id);
        $this->db->update($this->table_name, $data);
    }
    
    function delete($id) {
        $this->db->where($this->pk, $id);
        $this->db->delete($this->table_name);
    }

    function getMax($data) {
        $this->db->select_max($data);
        $query = $this->db->get($this->table_name);
        return $query->result()[0]->$data;
    }
}