<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/*
 * Version: 0.9
 */

class Convert {

    public function __construct() {
        $this->load->library('upload');
        $this->load->library('image_lib');
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    public function save($docid){
        $config                   = array();
        $config['allowed_types']  = 'pdf';
        $config['overwrite']      = TRUE;
        $config['remove_spaces']  = TRUE;

        $file = pathinfo($_FILES['file']['name']);

        $target_path = "extras/upload/karyailmiah/".$docid."/";

        $uploadPath = $target_path;
        $uploadOriPath = $target_path."ori/";
        $uploadWmPath = $target_path."wm/";

        if (!is_dir($uploadPath)) mkdir('./'.$uploadPath, 0777, true);
        if (!is_dir($uploadOriPath)) mkdir('./'.$uploadOriPath, 0777, true);
        if (!is_dir($uploadWmPath)) mkdir('./'.$uploadWmPath, 0777, true);

        $config['upload_path'] = base_url($uploadPath);
//        UPLOAD
        $filename3 = $this->unggah->upload_files('file', "", $uploadPath); // without ex
        unlink($uploadPath."index.html");
        $config['file_name'] = $filename3;

        $this->upload->initialize($config);

        if (! $filename3) return array('error' => $this->upload->display_errors());

        // converting pdf to images with imagick
        $im             = new Imagick();
        $im->setResolution(160,220);
        $im2             = new Imagick($this->config->item("doc_url").$uploadPath.$config['file_name']);
        $totalpage = $im2->getNumberImages();
        $ig = 1;

        for($i=0 ; $i<$totalpage ;$i++) {
            try {
                $im->readimage($this->config->item("doc_url") . $uploadPath . $config['file_name'] . "[" . $i . "]");
            } catch (Exception $e) {
                var_dump($e);
                die;
            }

            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(imagick::ALPHACHANNEL_REMOVE);
            $im->mergeImageLayers(imagick::LAYERMETHOD_FLATTEN);
            $im->setImageFormat('jpg');

            $image_name = $ig++ . '.jpg';

            $im->writeImage($this->config->item("doc_url") . $uploadOriPath . $image_name);
            $im->clear();
            $im->destroy();
        }

//        COPY n WATERMARK
        $this->wm($uploadOriPath, $uploadWmPath);
        return $totalpage;
//        return(json_encode(array(
//            'data' => 'Success',
//            'status' => 'success',
//            'code' => '200'
//        )));
    }

    public function wm($uploadOriPath, $uploadWmPath)
    {
        $pathIMGnya = $this->config->item("doc_url").$uploadOriPath;
// Loop on all JPG image exists in a folder
        if ($handle = opendir($pathIMGnya)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {

                    if ($this->endsWith($entry,'.jpg')==0){
                        // Skip images that are not JPG
//                        echo   "<br/> $entry: is ignored.\n  ";
                        continue;
                    }
                    // Just printing the image file name
//                    echo "<br/>".$uploadWmPath.$entry."<br/>  ";

// setting the image path
                    $image_path = $pathIMGnya.$entry;

// creating png image of watermark
                    $watermark = imagecreatefrompng($this->config->item("doc_url")."extras/convert/stamp.png");

// getting dimensions of watermark image
                    $watermark_width = imagesx($watermark);
                    $watermark_height = imagesy($watermark);

                    $image = imagecreatefromjpeg($image_path);
//something went wrong
                    if ($image === false) return false;
// getting the dimensions of original image
                    $tragetedImageSize = getImageSize($image_path);
// placing the watermark 6px from bottom and right
                    $wmPositionX = $tragetedImageSize[0]- ($watermark_width/2) - ($tragetedImageSize[0]/2);
                    $wmPositionY = $tragetedImageSize[1]- ($watermark_height/2) - ($tragetedImageSize[1]/2);
// blending the images together
                    imagealphablending($image, true);
                    imagealphablending($watermark, true);
// creating the new image with watermark on it,
                    imagecopy($image, $watermark, $wmPositionX, $wmPositionY, 0, 0, $watermark_width, $watermark_height);

// saving the created image into current directory
                    imagejpeg($image, $this->config->item("doc_url").$uploadWmPath.$entry);
// Freeing  memory
                    imagedestroy($image);
                    imagedestroy($watermark);

                }
            }
            closedir($handle);
        }
    }

    function endsWith($currentString, $target)
    {
        $length = strlen($target);
        if ($length == 0) {
            return true;
        }

        return (substr($currentString, -$length) === $target);
    }






    public function save_original_web()
    {
//        $config                   = array();
//        $config['allowed_types']  = 'pdf';
//        $config['overwrite']      = TRUE;
//        $config['remove_spaces']  = TRUE;
//
//        $this->load->library('upload', $config);
//
//// Image manipulation library
//        $this->load->library('image_lib');
//
//        foreach ($notes['name'] as $key => $note)
//        {
//            $_FILES['notes']['name']      = $notes['name'][$key];
//            $_FILES['notes']['type']      = $notes['type'][$key];
//            $_FILES['notes']['tmp_name']  = $notes['tmp_name'][$key];
//            $_FILES['notes']['error']     = $notes['error'][$key];
//            $_FILES['notes']['size']      = $notes['size'][$key];
//
//            $extension                    = pathinfo($_FILES['notes']['name'], PATHINFO_EXTENSION);
//            $unique_no                    = uniqid(rand(), true);
//            $filename[$key]               = $unique_no.'.'.$extension; // with ex
//            $filename2[$key]              = $unique_no; // without ex
//
//            $target_path                  = "notes_files/";
//
//            if (!is_dir($target_path))
//            {
//                mkdir('./'.$target_path, 0777, true);
//            }
//
//            $config['file_name']          = $filename[$key];
//            $config['upload_path']        = './'.$target_path;
//
//            $this->upload->initialize($config);
//
//            if (! $this->upload->do_upload('notes'))
//            {
//                return array('error' => $this->upload->display_errors());
//            }
//
//            // converting pdf to images with imagick
//            $im             = new Imagick();
//            $im->setResolution(160,220);
//
//            $ig = 0;
//
//            while(true)
//            {
//                try {
//                    $im->readimage($config['upload_path'].$config['file_name']."[$ig]");
//                } catch (Exception $e) {
//                    $ig = -1;
//                }
//
//                if($ig === -1) break;
//
//                $im->setImageBackgroundColor('white');
//                $im->setImageAlphaChannel(imagick::ALPHACHANNEL_REMOVE);
//                $im->mergeImageLayers(imagick::LAYERMETHOD_FLATTEN);
//                $im->setImageFormat('jpg');
//
//                $image_name     = $filename2[$key]."_$ig".'.jpg';
//                $imageprops     = $im->getImageGeometry();
//
//                $im->writeImage($config['upload_path'] .$image_name);
//                $im->clear();
//                $im->destroy();
//
//                // change file permission for file manipulation
//                chmod($config['upload_path'].$image_name, 0777); // CHMOD file
//
//                // add watermark to image
//                $img_manip              = array();
//                $img_manip              = array(
//                    'image_library'     => 'gd2',
//                    'wm_type'           => 'overlay',
//                    'wm_overlay_path'   => FCPATH . '/uploads/institutes/'.$institute_logo, // path to watermark image
//                    'wm_x_transp'       => '10',
//                    'wm_y_transp'       => '10',
//                    'wm_opacity'        => '10',
//                    'wm_vrt_alignment'  => 'middle',
//                    'wm_hor_alignment'  => 'center',
//                    'source_image'      => $config['upload_path'].$image_name
//                );
//
//                $this->image_lib->initialize($img_manip);
//                $this->image_lib->watermark();
//
//                ImageJPEG(ImageCreateFromString(file_get_contents($config['upload_path'].$image_name)), $config['upload_path'].$image_name, );
//
//                $ig++;
//            }
//
//            // unlink the original pdf file
//            chmod($config['upload_path'].$config['file_name'], 0777); // CHMOD file
//            unlink($config['upload_path'].$config['file_name']);    // remove file
//        }
//// echo '<p>Success</p>';exit;
//        die(json_encode(array(
//            'data' => 'Success',
//            'status' => 'success'
//        )));
    }

}
