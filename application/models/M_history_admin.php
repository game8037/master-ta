<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_history_admin extends MY_Model {

    var $table_name = "history_admin";
    var $pk = "idlog";


    function getCount($ktiid) {
        $this->db->where("karyailmiahid", $ktiid);
        $query = $this->db->get($this->table_name);
        return count($query->result());
    }

}