<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_history_user extends MY_Model {

    var $table_name = "history_user";
    var $pk = "id";


    function getCount($ktiid) {
        $this->db->where("karyailmiahid", $ktiid);
        $query = $this->db->get($this->table_name);
        return count($query->result());
    }

    function getTop() {
        $this->db->select("history_user.karyailmiahid, COUNT(history_user.id) jml ");
        $this->db->join("karyailmiah k", "k.id = history_user.karyailmiahid");
        $this->db->group_by("history_user.karyailmiahid");
        $this->db->order_by("jml", "DESC");
        $this->db->limit(10);
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

}