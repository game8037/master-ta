<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	var $kelas = "Login";

	function __construct(){
		parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
	}

	public function index($status = "0"){
		if ($this->input->post('btnsubmit')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$cek = $this->M_user->login($username,$password);
			if($cek){
				$this->session->set_userdata("id",$cek->userid);
				$this->session->set_userdata("error",false);
				$this->session->set_userdata("welcome",TRUE);
                redirect("welcome");
			}

			$this->session->set_userdata("error","Username / Password yang anda masukan salah");
		}

		$data["status"] = $status;
		$this->load->view("page/login",$data);
	}

	public function changepassword(){
        $id = $this->session->userdata("id");
		if ($this->input->post('btnsubmit')) {
			$password0 = $this->input->post('password0');
			$password1 = $this->input->post('password1');
			$password2 = $this->input->post('password2');

//			CEK KOMBINASI
//            $pass="paswdT6Sa";
//            $uppercase = preg_match('@[A-Z]@', $pass);
//            $lowercase = preg_match('@[a-z]@', $pass);
//            $number    = preg_match('@[0-9]@', $pass);
//
//            if(!$uppercase || !$lowercase || !$number || strlen($pass)<=6){
//                echo "password harus lebih dari 6 karakter, mengandung huruf BESAR, huruf kecil dan angka";
//            }else{
//                echo "password memenuhi kriteria";
//            }

//			CEK OLD PASSWORD
			if($password0 == $this->encrypt->decode($this->M_user->getDetail($id)->password)){
                if($password1 == $password2){
                    $this->M_user->update($id,array("password" => $this->encrypt->encode($password1)));
                    $this->session->set_flashdata("success","Password berhasil diubah");
                }
                else{
                    $this->session->set_flashdata("warning","New dan Confirm Password tidak sama");
                }
            }
            else{
                $this->session->set_flashdata("warning","Old Password tidak sama");
            }
        }
		$this->load->view("page/change");
	}

	public function logout(){		

		$this->session->unset_userdata("id");
		$this->session->unset_userdata("error");
		$this->session->unset_userdata("welcome");
		redirect("Welcome");
	}
}
