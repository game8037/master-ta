<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends CI_Controller {
	
	var $kelas = "Jurusan";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_jurusan->getAll();
		$data['konten'] = "jurusan/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_jurusan->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["nama"] = $this->input->post("nama");
		
		if($id) {
            $this->M_jurusan->update($id,$data);
            $this->jejak->add($this->user->userid, "Mengubah Jurusan ".$data['nama'], "Jurusan/index");
        }
		else {
            $this->M_jurusan->add($data);
            $this->jejak->add($this->user->userid, "Menambah Jurusan ".$data['nama'], "Jurusan/index");
        }

		redirect($this->kelas);
	}

	public function delete($id){
        $jurusan = $this->M_jurusan->getDetail($id);
        $this->M_jurusan->delete($id);
        $this->jejak->add($this->user->userid, "Menghapus Jurusan ".$jurusan->nama, "Jurusan/index");
        redirect($this->kelas);
	}
}
