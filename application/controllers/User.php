<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    var $kelas = "User";

    function __construct(){
        parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
    }

    public function index(){
        $data["rowData"] = $this->M_user->getAll();
        $data['konten'] = "user/index";
        $this->load->view('template',$data);
    }

    public function detail($id){
        header('Content-Type: application/json');
        $rowData = $this->M_user->getDetail($id);
        echo json_encode( $rowData );
    }

    public function add(){
        $id = $this->input->post("userid");
        $data["noinduk"] = $this->input->post("noinduk");
        $data["fullname"] = $this->input->post("fullname");
        $data["roleid"] = $this->input->post("roleid");
        $data["password"] = $this->encrypt->encode("xyz123456");

        if($id){
            $this->M_user->update($id,$data);
            $this->jejak->add($this->user->userid, "Mengubah User ".$data["nama"], "User/index");
        }
        else{
            $this->M_user->add($data);
            $this->jejak->add($this->user->userid, "Menambah User ".$data["nama"], "User/index");
        }

        redirect($this->kelas);
    }

    public function resetpassword($id)
    {
        $data["password"] = $this->encrypt->encode("xyz123456");

        $this->M_user->update($id,$data);
        $this->jejak->add($this->user->userid, "Mereset Password User ".$data["nama"], "User/index");

        redirect($this->kelas);
    }

    public function delete($id){
        $user = $this->M_user->getDetail($id);
        $this->M_user->delete($id);
        $this->jejak->add($this->user->userid, "Menghapus User ".$user->fullname, "User/index");
        redirect($this->kelas);
    }
}
